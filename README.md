# Leaflet.Polyline.DashFlow

A plugin for [LeafletJS](http://www.leafletjs.com) to animate the dashing of dashed lines.


![Screencapture GIF](demo.gif)

Only tested with Leaflet 1.4.0. See the [demo](https://ivansanchez.gitlab.io/Leaflet.Path.DashFlow/demo.html).

### API

New option in all `L.Path`s (`L.Polyline`s, `L.Polygon`s, `L.Rectangle`s, `L.Circle`s, `L.CircleMarker`s): `dashSpeed`. Give it a value in pixels per second - this is how fast the dashing will move.

```js
var line = L.polyline(latlngs, {dashArray: "15 15", dashSpeed: 30});
line.addTo(map);
```

### Legalese

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
